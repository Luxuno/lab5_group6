import 'package:flutter/material.dart';
import 'package:lab5_group6/settingsTab.dart';
import 'timelineTab.dart';
import 'timelineListings.dart';
import 'profileTab.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Color.fromRGBO(15, 15, 15, 1),
          appBar: AppBar(
            title: Text('Working Title'),
            automaticallyImplyLeading: true,
            bottom: TabBar(
              indicatorColor: Colors.white,
              tabs: [
                Tab(text: 'Timeline', icon: Icon(Icons.chat)),
                Tab(text: 'Profile', icon: Icon(Icons.person)),
                Tab(text: 'Settings', icon: Icon(Icons.settings),),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              timelineColumn(row1img, row1titles, row1values),  //found in TimelineTab.dart
              profileColumn(),
              settingsColumn(),
            ],
          ),
        ),
      ),
    );
  }
}
