import 'package:flutter/material.dart';

class profileColumn extends StatefulWidget {

  @override
  _profileColumnState createState() => _profileColumnState();
}

class _profileColumnState extends State<profileColumn> {
  @override
  Widget build(BuildContext context) {
    return (
    Container(
      child: DefaultTextStyle(
        style: TextStyle(color: Colors.white,),
        textAlign: TextAlign.left,
        child: Column(
          children: [
            DecoratedBox(
              child: (
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset('images/defaultUser.jpg',
                    height: 200,),
                    Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: 32),
                          child: Text('1337Gam3r', style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Text('@jimmypesto24'),
                        ),
                        Text('Ocean Avenue, NY')
                      ],
                    )
                  ])
              ),
              decoration: BoxDecoration(
                color: Colors.white10,
                border: Border.all(
                  color: Colors.white,
                  width: 2
                ),
                borderRadius: BorderRadius.circular(8)
              ),

            ),

          ],
        )
      ),
    )
    );
  }
}

