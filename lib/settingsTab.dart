import 'package:flutter/material.dart';

class settingsColumn extends StatefulWidget {

  @override
  _settingsColumnState createState() => _settingsColumnState();
}


class _settingsColumnState extends State<settingsColumn> {

  double fontSize = 16;
  FontWeight fontWeight = FontWeight.w500;
  Color buttonColor = Colors.black;
  Color textColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        _buildSettingsButton('Account', 'Personal account settings', Icons.account_circle,
            fontSize, fontWeight, textColor, buttonColor),
        _buildSettingsButton('Brightness', 'Set display brightness', Icons.brightness_6,
            fontSize, fontWeight, textColor, buttonColor),
        _buildSettingsButton('Language', 'Set app language', Icons.translate,
            fontSize, fontWeight, textColor, buttonColor),
        _buildSettingsButton('About', 'App information', Icons.info,
            fontSize, fontWeight, textColor, buttonColor),
        _buildSettingsButton('Feedback', 'Send feedback to the developers', Icons.feedback,
            fontSize, fontWeight, textColor, buttonColor),
        _buildSettingsButton('Help', 'Issues with the application?', Icons.help,
            fontSize, fontWeight, textColor, buttonColor),

      ],
    );
  }

  FlatButton _buildSettingsButton(String label, String message, IconData icon,
      double fontSize, FontWeight weight, Color fontColor, Color buttonColor)
  {
    return FlatButton(
        height: 70,
        child: Align(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //SizedBox(width: 20,),
              Text(label,
                style: TextStyle(fontSize: fontSize, fontWeight: weight, color: fontColor),
              ),
              Tooltip(
                message: message,
                child: Icon(icon, color: fontColor,),
              ),
            ],
          ),
          alignment: Alignment.centerLeft,
        ),
        hoverColor: Colors.grey,
        splashColor: Colors.grey[800],
        color: buttonColor,
        onPressed: () {
          setState(() { });
        }
    );
  }
}