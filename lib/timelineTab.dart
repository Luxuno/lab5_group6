import 'dart:ui';

import 'package:flutter/material.dart';
import 'timelineListings.dart';


/*
* Development Notes
*
* This was really fun to make.
*
* Includes:
* - Cards containing the concept post (Image/Title/Like/Comment/Follow)
* - Clickable images! Increases image to the width of screen
* - Working chips that add index to a list called likeList and followThreadList (For both colors/numbers and other possible features)
* - Tooltips when long pressing chips
* - Snackbar pop-up when pressing any of the chips
*
* Sidenotes:
* - Images are from previous lab (Lab 4) as the concept was similar
* - Code is a variation of my previous listview.builder from the previous lab as well, but heavily modified to be complete
* */


class timelineColumn extends StatefulWidget {
  List<String> imageItems;
  List<String> titleItems;
  List<List> items;

  timelineColumn(this.imageItems, this.titleItems, this.items);

  @override
  _timelineColumnState createState() => _timelineColumnState();
}

//Contains the Daily Top Pictures Row
class _timelineColumnState extends State<timelineColumn> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //height: 350,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: widget.imageItems.length,
        itemBuilder: (BuildContext context, int index) => Column(
          children: [
            Container(
              padding: EdgeInsets.all(5),
              //width: 225,
              //Color of the card and its appearance
              child: Card(
                color: Color.fromRGBO(28, 28, 28, 1),
                //elevation: 8,
                shadowColor: Color.fromRGBO(38, 38, 38, 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    //Image asset
                    GestureDetector(
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        child:Image.asset(
                            widget.imageItems[index],
                            fit: BoxFit.fill,
                          ),
                      ),
                      onTap: () {
                        showDialog(
                          barrierColor: Color.fromRGBO(0, 0, 0, 0.94),
                            context: context,
                            builder: (BuildContext context) {
                              return GestureDetector(
                                child: Image.asset(
                                  widget.imageItems[index],
                                  fit: BoxFit.fitWidth,
                                ),
                                onTap: () {Navigator.pop(context);},
                              );
                            });
                      },
                    ),
                    SizedBox(height: 10),
                    //OP's comment
                    Text(
                      widget.titleItems[index],
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10),
                    //Contains
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [

                        //Like Button
                        ActionChip(
                            //shape: RoundedRectangleBorder(),
                          tooltip: 'Like post?',
                            shape: StadiumBorder(side: BorderSide(color: Colors.white)),
                            backgroundColor: likeList.contains(index)
                                ? Colors.deepOrangeAccent
                                : Color.fromRGBO(26, 26, 26, 1),
                            label: Text(
                              widget.items[index][0].toString(),
                              style: TextStyle(
                                  color: widget.items[index][0] > 100 && !likeList.contains(index) ? Color.fromRGBO(245, 201, 91, 1) : Colors.white,
                              ),
                            ),
                            avatar: Icon(
                              Icons.arrow_upward,
                              color: likeList.contains(index)
                              ? Color.fromRGBO(217, 17, 17, 1)
                              : Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                String interchangable;

                                if (likeList.contains(index)) {
                                  likeList.remove(index);
                                  widget.items[index][0] -= 1;
                                  interchangable = 'Removed like from post!';
                                } else {
                                  likeList.add(index);
                                  widget.items[index][0] += 1;
                                  interchangable = 'Liked a post!';
                                }

                                final showSnackBar = SnackBar(
                                  content:
                                  Text(interchangable),
                                  duration: const Duration(seconds: 1, milliseconds: 500),
                                  backgroundColor:
                                  Colors.black,
                                  behavior: SnackBarBehavior.floating,
                                );

                                Scaffold.of(context).showSnackBar(showSnackBar);

                                print('likelist');
                                print(likeList);
                              });
                            }),

                        //Comment Thread Button
                        ActionChip(
                          tooltip: 'Leave a comment?',
                            shape: StadiumBorder(side: BorderSide(color: Color.fromRGBO(161, 68, 61, 1))),
                            backgroundColor: Color.fromRGBO(26, 26, 26, 1),
                            label: Text(
                              widget.items[index][1].toString(),
                              style: TextStyle(color: Colors.white),
                            ),
                            avatar: Icon(
                              Icons.textsms,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                final showSnackBar = SnackBar(
                                  content:
                                      Text('Threads are locked at this time'),
                                  action: SnackBarAction(
                                    label: 'Dismiss',
                                    onPressed: () {},
                                  ),
                                  duration: const Duration(seconds: 3),
                                  backgroundColor: Colors.black,
                                  behavior: SnackBarBehavior.floating,
                                );

                                Scaffold.of(context).showSnackBar(showSnackBar);
                              });
                            }),

                        //Follow Post Button
                        ActionChip(
                          tooltip: 'Follow post?',
                            shape: StadiumBorder(side: BorderSide(color: Colors.white)),
                            backgroundColor: followThreadList.contains(index)
                                ? Colors.lightBlue
                                : Color.fromRGBO(26, 26, 26, 1),
                            label: Text(
                              widget.items[index][2].toString(),
                              style: TextStyle(color: Colors.white),
                            ),
                            avatar: Icon(
                              Icons.remove_red_eye,
                              color: followThreadList.contains(index)
                                  ? Color.fromRGBO(51, 17, 217, 1)
                                  : Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                String interchangable;

                                if (followThreadList.contains(index)) {
                                  followThreadList.remove(index);
                                  widget.items[index][2] -= 1;
                                  interchangable = 'Unfollowed a post';
                                } else {
                                  followThreadList.add(index);
                                  widget.items[index][2] += 1;
                                  interchangable = 'Followed a post';
                                }

                                final showSnackBar = SnackBar(
                                  content:
                                  Text(interchangable),
                                  duration: const Duration(seconds: 1, milliseconds: 500),
                                  backgroundColor: Colors.black,
                                  behavior: SnackBarBehavior.floating,

                                );

                                Scaffold.of(context).showSnackBar(showSnackBar);

                                print('followThreadList');
                                print(followThreadList);
                              });
                            }),
                      ],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
