import 'dart:math';
var rng = new Random();

List<String> row1img = [
  'images/coolio.PNG',
  'images/Y65t9QS.png',
  'images/fullOrientalCat.png',
  'images/PYROMANCY.png',
  'images/TerrariaAircraftCarrier.png',
  'images/gameDevProgress.jpg',
  'images/programmerMeme.jpg',
  'images/cursedImage.jpg',
  'images/stonksIsReal.jpg',
  'images/Amazing..jpg',
  'images/connectFork.jpg',
  'images/derpyCat.jpg',
  'images/EEEEEHHHHHH.jpg',
  'images/expectNothing.jpg',
  'images/faces.jpg',
  'images/glockOnACroc.jpg',
  'images/itsMyStyle.jpg',
  'images/moneyDog.jpg',
  'images/nowthisispodracing.png',
  'images/PepeSilvia.png',
  'images/soaked.png',
  'images/SpongeMarines.jpg',
  'images/superNintendoChalmers.jpg',
  'images/donkeyBonk.png',
  'images/greenLanternGuy.jpg',
  'images/noob.jpg',
  'images/wholesomeAliens.jpg',
];

List<String> row1titles = [
  'Finally reached level 80~!',
  'Windwaker Lonk',
  'Oriental-styled Smudge the cat',
  'Elmos NEW world',
  'My aircraft carrier build in Terraria',
  'New character for my game dev project',
  'Programmers these days',
  'Cursed keyboard',
  'I thought he looked familiar',
  'If Rob Liefeld directed Captain America',
  'Connect Fork',
  'My derpy cat',
  'Dude, chill',
  'Totally not a repost',
  'A face in my backpack',
  'Forget Elf on a Shelf, get ready for...',
  'When they say "its just my style" ',
  'This is Money Dog',
  'Totally not another repost',
  'When it all starts to come together',
  'Dunk me',
  'Sponge Marines',
  'Super Nintendo Chalmers',
  'Donkey Bonk',
  'This funny Green Lantern Guy',
  'Why is that even there?!',
  'Wholesome Aliens playing video games',
];

int num1;

List<List> row1values = [
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
  [num1 = rng.nextInt(100)+5,rng.nextInt(100),rng.nextInt(num1)],
];

int randomGen()
{
  return rng.nextInt(100);
}

List<int> likeList = [];
List<int> followThreadList = [];